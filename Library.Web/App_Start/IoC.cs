﻿using System;
using System.Linq;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Library.Common;
using Library.Common.IServices;
using Library.Services;
using Library.Web.Controllers;

namespace Library.Web.App_Start
{
    public class IoC
    {
        public static void RegisterDependencies(ContainerBuilder builder)
        {
            //var builder = new ContainerBuilder();
            //builder.RegisterControllers(typeof(MvcApplication).Assembly);

            /*
             * Rejestracja Kontrolerów.
             */
            builder.RegisterType<UserController>().InstancePerRequest();
            builder.RegisterType<BorrowController>().InstancePerRequest();
            builder.RegisterType<BookController>().InstancePerRequest();
            builder.RegisterType<StatisticsController>().InstancePerDependency();
            //builder.RegisterType<UnitOfWork>().

            /*
             * Rejestracja Serwisów.
             */
            
            //builder.RegisterType<IUnitOfWork>().As<IUserService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<BookService>().As<IBookService>();
            builder.RegisterType<BorrowService>().As<IBorrowService>();
            builder.RegisterType<StatisticsService>().As<IStatisticsService>();

            /*
             * Rejestracja UoW.
             */
            builder.RegisterType<UnitOfWork>().InstancePerRequest();

            
            IContainer container = builder.Build();
            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}