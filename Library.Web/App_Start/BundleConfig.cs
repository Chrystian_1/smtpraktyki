﻿using System;
using System.Linq;
using System.Web.Optimization;

namespace Library.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            string jqueryVersion = "1.9.1";
            bundles.UseCdn = true;
            BundleTable.EnableOptimizations = true;
            
            string jqueryCdnPath = string.Format("{0}{1}/jquery-ui.js", @"https://code.jquery.com/ui/", jqueryVersion);
            string jqueryUiCdnPath = string.Format("{0}{1}.js", @"https://code.jquery.com/jquery-", jqueryVersion);
            
            bundles.Add(new ScriptBundle("~/bundles/jquery", jqueryCdnPath).Include(
                string.Format("~/Scripts/jquery-{0}.js", jqueryVersion)));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                string.Format("~/Scripts/jquery-ui-{0}.js", jqueryUiCdnPath)));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));
            
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                "~/Content/themes/base/jquery.ui.core.css",
                "~/Content/themes/base/jquery.ui.resizable.css",
                "~/Content/themes/base/jquery.ui.selectable.css",
                "~/Content/themes/base/jquery.ui.accordion.css",
                "~/Content/themes/base/jquery.ui.autocomplete.css",
                "~/Content/themes/base/jquery.ui.button.css",
                "~/Content/themes/base/jquery.ui.dialog.css",
                "~/Content/themes/base/jquery.ui.slider.css",
                "~/Content/themes/base/jquery.ui.tabs.css",
                "~/Content/themes/base/jquery.ui.datepicker.css",
                "~/Content/themes/base/jquery.ui.progressbar.css",
                "~/Content/themes/base/jquery.ui.theme.css"));

            string kendoVersion = "2015.2.624";
            string kendoCommonCdnPath = @"http://cdn.kendostatic.com//styles/kendo.common-bootstrap.min.css";
            string kendoBootstrapCdnPath = string.Format("{0}{1}{2}", @"http://cdn.kendostatic.com/", kendoVersion, @"/styles/kendo.bootstrap.min.css");
            string kendoAllMinCdnPath = string.Format("{0}{1}{2}", @"http://cdn.kendostatic.com/", kendoVersion, @"/js/kendo.all.min.js");
            string kendoAspNetMvcCdnPath = string.Format("{0}{1}{2}", @"http://cdn.kendostatic.com/", kendoVersion, @"/js/kendo.timezones.min.js");
            string kendoTimeZonesCdnPath = string.Format("{0}{1}{2}", @"http://cdn.kendostatic.com/", kendoVersion, @"/js/kendo.aspnetmvc.min.js");

            bundles.Add(new StyleBundle("~/bundles/kendo", kendoCommonCdnPath));
            bundles.Add(new StyleBundle("~/bundles/kendo", kendoBootstrapCdnPath));
            bundles.Add(new ScriptBundle("~/bundles/kendo", kendoAllMinCdnPath));
            bundles.Add(new ScriptBundle("~/bundles/kendo", kendoAspNetMvcCdnPath));
            bundles.Add(new ScriptBundle("~/bundles/kendo", kendoTimeZonesCdnPath));
            //.Include("~/Scripts/kendo.common-bootsstrap.min.css"));
        }
    }
}