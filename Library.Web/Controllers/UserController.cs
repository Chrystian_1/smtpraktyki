﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Library.Common.IServices;
using Library.Common.ViewModels.User;

namespace Library.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService _userService)
        {
            this.userService = _userService;
        }

        //
        // GET: /User/
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<UserViewModel> userList = this.userService.GetUsers();
            return this.View(userList);
        }

        //
        // GET: /User/Details/5
        [HttpGet]
        public ActionResult Details(int id)
        {
            UserDetails user = this.userService.GetUserDetails(id);
            return this.View(user);
        }

        //
        // GET: /User/CreateC:\Users\chrystian.kislo\Documents\Visual Studio 2012\Projects\LibraryProject\Library.Web\Controllers\UserController.cs
        [HttpGet]
        public ActionResult Create()
        {
            return this.View();
        }

        //
        // POST: /User/Create
        [HttpPost]
        public ActionResult Create(NewUserModel newUser)
        {
            this.userService.AddUser(newUser);
            return this.RedirectToAction("Index");
        }

        //
        // GET: /User/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            EditUserModel user = this.userService.GetUser(id);
            return this.View(user);
        }

        //
        // POST: /User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditUserModel editedUser)
        {
            editedUser.UserId = id;
            this.userService.EditUser(editedUser);
            return this.RedirectToAction("Index");
        }

        //
        // GET: /User/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            UserDetails user = this.userService.GetUserDetails(id);
            return this.View(user);
        }

        //
        // POST: /User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            this.userService.DeleteUser(id);
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult UserInfoDetails(int id)
        {
            UserInfoDetailsModel userDetails = this.userService.GetBookInfoDetails(id);
            return this.PartialView(userDetails);
        }
    }
}
