﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Library.Common.Exceptions;
using Library.Common.IServices;
using Library.Common.ViewModels.Book;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.DictBookGenre;
using Library.Common.Extensions;

namespace Library.Web.Controllers
{
    public class BorrowController : Controller
    {
        private readonly IBookService bookService;
        private readonly IBorrowService borrowService;
        private readonly IUserService userService;

        public BorrowController(IBookService bookService,
            IBorrowService borrowService,
            IUserService userService)
        {
            this.bookService = bookService;
            this.borrowService = borrowService;
            this.userService = userService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BorrowedBooks()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BorrowBook()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult BorrowBooks(int userId, int[] books)
        {
            try
            {
                borrowService.BorrowBooks(userId, books);
            }
            catch (UserActiveException ex)
            {
                return this.ResponseFailureJson(ex.Message);
            }
            catch (BorrowException ex)
            {
                return this.ResponseFailureJson(ex.Message);
            }
            return this.ResponseSuccessJson();
        }

        [HttpPost]
        public ActionResult BookBorrowedJson([DataSourceRequest]
                                             DataSourceRequest request)
        {
            IQueryable<BookBorrowedByUserModel> books = this.bookService.GetBorrowedBooks();
            return this.Json(books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetBorrowedBooksByUserJson([DataSourceRequest]
                                                       DataSourceRequest request, int id)
        {
            IQueryable<BookBorrowedReturnModel> books = this.borrowService.GetBorrowedBooksByUser(id);
            return this.Json(books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAviableBooksForUser(int id)
        {
            var books = this.bookService.GetAviableBooksForUser(id);
            IEnumerable<SelectListItem> booksEnumerable = 
                books.Select(c => new SelectListItem { Text = c.Value, Value = c.Key.ToString() }).AsEnumerable();
            if (booksEnumerable.FirstOrDefault() == null)
            {
                return this.ResponseFailureJson("No records for this user!");
            }
            return this.ResponseSuccessJson(this.Json(booksEnumerable));
        }

        [HttpGet]
        public ActionResult GetAviableUsers([DataSourceRequest]
                                            DataSourceRequest request)
        {
            var users = this.userService.GetAviableUsers();
            IEnumerable<SelectListItem> usersEnumerable = users.Select(c => new SelectListItem { Text = c.Key.ToString(), Value = c.Value }).AsEnumerable();
            return Json(usersEnumerable, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UsersWithBooks()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UsersWithBooksJson([DataSourceRequest]
                                               DataSourceRequest request)
        {
            IQueryable<UserBorrowCountModel> books = this.userService.GetUsersWithBooks();
            return this.Json(books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnBook(int id)
        {
            this.borrowService.ReturnBook(id);
            return ResponseSuccessJson();
        }

        [HttpPost]
        public ActionResult ReturnBooks(int[] id)
        {
            if (id == null)
            {
                return ResponseJson("nonBooks", "Zaznacz se jakies ksiazki!");
            }
            this.borrowService.ReturnBooks(id);
            return ResponseSuccessJson();
        }

        private ActionResult ResponseJson(string status, string message)
        {
            return this.Json(new
            {
                status = status,
                message = message
            });
        }

        private ActionResult ResponseFailureJson(string message)
        {
            return this.ResponseJson("failure", message);
        }

        private ActionResult ResponseSuccessJson(string message)
        {
            return this.ResponseJson("success", message);
        }

        private ActionResult ResponseSuccessJson()
        {
            return this.ResponseJson("success", string.Empty);
        }

        private ActionResult ResponseSuccessJson(JsonResult json)
        {
            return this.Json(new
            {
                status = "success",
                message = json
            });
        }
    }
}