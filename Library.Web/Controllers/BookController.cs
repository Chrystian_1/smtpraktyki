﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Library.Common.IServices;
using Library.Common.ViewModels.Book;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.DictBookGenre;

namespace Library.Web.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService bookService;

        public BookController(IBookService bookService)
        {
            this.bookService = bookService;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
        }

        //
        // GET: /Book/
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<BookViewModel> bookList = this.bookService.GetBooks();
            return this.View(bookList);
        }

        [HttpPost]
        public ActionResult BookListJson([DataSourceRequest]
                                         DataSourceRequest request)
        {
            IEnumerable<BookViewModel> bookList = this.bookService.GetBooks();
            return this.Json(bookList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
            
        [HttpPost]
        public ActionResult GetBookStatus(int id, [DataSourceRequest]
                                          DataSourceRequest request)
        {
            IQueryable<UserBorrowInfoModel> history = this.bookService.GetBookStatus(id);
            return this.Json(history.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetBookHistory(int id, [DataSourceRequest]
                                          DataSourceRequest request)
        {
            IQueryable<UserHistoryBorrowInfoModel> history = this.bookService.GetBookHistory(id);
            return this.Json(history.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BookInfoDetails(int id)
        {
            BookInfoDetailsModel bookDetails = this.bookService.GetBookInfoDetails(id);
            return this.PartialView(bookDetails);
        }

        //
        // GET: /Book/Details/5
        [HttpGet]
        public ActionResult Details(int id)
        {
            BookDetailsModel bookDetails = this.bookService.GetBookDetails(id);
            return this.View(bookDetails);
        }

        //
        // GET: /Book/Create
        [HttpGet]
        public ActionResult Create()
        {
            return this.PartialView();
        }

        //
        // POST: /Book/Create
        [HttpPost]
        public ActionResult Create(BookAddModel newBook)
        {
            if (this.ModelState.IsValid)
            {
                int id = this.bookService.AddBook(newBook);
                return ResponseSuccesssJson(id);
            }
            return this.PartialView(newBook);
        }

        //
        // GET: /Book/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            BookEditModel book = this.bookService.GetBook(id);
            return this.PartialView(book);
        }

        //
        // POST: /Book/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, BookEditModel editedBook)
        {
            if (this.ModelState.IsValid)
            {
                editedBook.BookId = id;
                if (!this.ValidateBookCount(id, editedBook.Count))
                {
                    return this.PartialView();
                }
                this.bookService.EditBook(editedBook);
                return ResponseSuccesssJson();
            }
            return this.PartialView();
        }
        
        private bool ValidateBookCount(int id, int bookCount)
        {
            int count = this.bookService.GetBookBorrowCount(
                id);
            
            if (count > bookCount)
            {
                this.ModelState.AddModelError("Count", String.Format("Number of count cannt be less then {0}.", count));
                return false;
            }
            return true;
        }
        private ActionResult RedirectJsonAjax(string redirectUrl)
        {
            return this.Json(new
            {
                redirectUrl = redirectUrl,
                isRedirect = true
            });
        }
        private ActionResult RedirectJsonAjax(string action, string controller)
        {
            return this.Json(new
            {
                redirectUrl = Url.Action(action, controller),
                isRedirect = true
            });
        }
        private ActionResult ResponseSuccesssJson()
        {
            return this.Json(new
            {
                isSuccess = true
            });
        }
        private ActionResult ResponseSuccesssJson(string message)
        {
            return this.Json(new
            {
                isSuccess = true,
                message = message
            });
        }
        private ActionResult ResponseSuccesssJson(int message)
        {
            return this.Json(new
            {
                isSuccess = true,
                message = message
            });
        }


    }
}