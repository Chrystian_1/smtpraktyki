﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Library.Common.IServices;

namespace Library.Web.Controllers
{
    public class StatisticsController : Controller
    {
        private readonly IStatisticsService statisticsService;

        public StatisticsController(IStatisticsService statisticsService)
        {
            this.statisticsService = statisticsService;
        }
        //
        // GET: /Statistics/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult BooksStatistics()
        {
            var books = statisticsService.GetBookOrderByBorrowsCount();
            return PartialView(books);
        }
        [ChildActionOnly]
        public ActionResult UsersStatistics()
        {
            var users = statisticsService.GetUsersOrderByBorrowsCount();
            return PartialView(users);

        }

        [ChildActionOnly]
        public ActionResult Filter()
        {
            return PartialView();

        }

        public ActionResult UsersLastsNames()
        {
            var userLastNames = statisticsService.GetUsersLastsNames();
            return Json(userLastNames, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetGenres()
        {
            var genres = this.statisticsService.GetGenres();
            IEnumerable<SelectListItem> genresEnumerable = genres
                .Select(c => new SelectListItem { Text = c.Value, Value = c.Key.ToString() }).AsEnumerable();
            return this.Json(genresEnumerable, JsonRequestBehavior.AllowGet);
        } 
        


        [HttpPost]
        public ActionResult BooksJson([DataSourceRequest]
                                         DataSourceRequest request)
        {
            var books = statisticsService.GetBookOrderByBorrowsCount();
            return this.Json(books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsersJson([DataSourceRequest]
                                         DataSourceRequest request)
        {
            var usres = statisticsService.GetUsersOrderByBorrowsCount();
            return this.Json(usres.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

	}
}