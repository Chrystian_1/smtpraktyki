﻿using System;
using System.Linq;

namespace Library.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string input)
        {
            if (input == null)
            {
                return false;
            }
            return string.IsNullOrEmpty(input);
        }
    }
}