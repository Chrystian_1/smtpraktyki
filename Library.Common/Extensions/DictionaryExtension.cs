﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.Extensions
{
    public static class DictionaryExtension
    {
        public static Dictionary<string, object> ToJsonDictionary<TKey, TValue>(this Dictionary<TKey, TValue> input)
        {
            var output = new Dictionary<string, object>(input.Count);
            foreach (KeyValuePair<TKey, TValue> pair in input)
                output.Add(pair.Key.ToString(), pair.Value);
            return output;
        } 
    }
}
