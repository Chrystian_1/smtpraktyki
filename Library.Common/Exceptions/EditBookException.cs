﻿using System;
using System.Linq;

namespace Library.Common.Exceptions
{
    [Serializable]
    public class EditBookException : Exception
    {
        public EditBookException()
        {
        }

        public EditBookException(int count) : base(String.Format("Number of count cannt be less then {0}.", count))
        {
            this.Count = count;
        }

        public EditBookException(int count, string message) : base(message)
        {
            this.Count = count;
        }

        public EditBookException(string message, Exception inner) : base(message, inner)
        {
        }

        protected EditBookException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public int Count { get; private set; }
    }
}