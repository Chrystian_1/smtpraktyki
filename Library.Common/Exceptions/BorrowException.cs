﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.Exceptions
{
    [Serializable]
    public class BorrowException : Exception
    {
        public BorrowException() : base("Sorry, but some errors occurred during server works. Maybe you try borrow this same book twice.") {
        }
        public BorrowException(string message) : base(message) { }
        public BorrowException(string message, Exception inner) : base(message, inner) { }
        protected BorrowException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
