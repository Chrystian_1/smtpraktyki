﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.Exceptions
{
    [Serializable]
    public class UserActiveException : Exception
    {
        public UserActiveException() : base("This user is unactive.") { }
        public UserActiveException(string message) : base(message) { }
        public UserActiveException(string message, Exception inner) : base(message, inner) { }
        protected UserActiveException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
