﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.ViewModels.User
{
    public class UserInfoDetailsModel
    {
        [Display(Name = "User Id")]
        public int UserId { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Birth Date")]
        public System.DateTime BirthDate { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        [Display(Name = "Add Date")]
        public System.DateTime AddDate { get; set; }

        [Display(Name = "Modified Date")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }
    }
}
