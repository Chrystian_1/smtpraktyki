﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.User
{
    public class UserViewModel
    {
        [Display(Name = "User Id")]
        public int UserId { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Birth Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        [Display(Name = "Add Date")]
        public DateTime AddDate { get; set; }

        [Display(Name = "Modified Date")]
        public Nullable<DateTime> ModifiedDate { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }
    }
}