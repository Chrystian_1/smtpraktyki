﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.User
{
    public class EditUserModel
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(30)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [Required]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }
    }
}