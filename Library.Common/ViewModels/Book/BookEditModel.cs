﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Library.Common.ViewModels.DictBookGenre;

namespace Library.Common.ViewModels.Book
{
    public class BookEditModel
    {
        [Required]
        public int BookId { get; set; }

        [Required]
        public string Author { get; set; }

        [Required]
        public string Title { get; set; }

        [Display(Name = "Relese Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ReleaseDate { get; set; }

        [Required]
        [StringLength(13)]
        public string ISBN { get; set; }

        //[Range(1, 5, ErrorMessage="Porszę wybrać gatunek :D")]
        [EnumDataType(typeof(BookGenre))]
        [Display(Name = "Book Genre")]
        [Required]
        public BookGenre BookGenre { get; set; }

        [Display(Name = "Count")]
        [Range(0, int.MaxValue)]
        public int Count { get; set; }
    }
}