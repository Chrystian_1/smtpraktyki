﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.ViewModels.Book
{
    public class BookInfoDetailsModel
    {
        [Display(Name = "Book Id")]
        public int BookId { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        [Display(Name = "Relese Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ReleaseDate { get; set; }

        public string ISBN { get; set; }

        [Display(Name = "Book Genre")]
        public string BookGenre { get; set; }

        [Display(Name = "Count")]
        public int Count { get; set; }

        [Display(Name = "Count Left")]
        public int CountLeft { get; set; }

        [Display(Name = "Count Borrow")]
        public int CountBorrow { get; set; }

        [Display(Name = "Add Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AddDate { get; set; }

        [Display(Name = "Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ModifiedDate { get; set; }
    }
}
