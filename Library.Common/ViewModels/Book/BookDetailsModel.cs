﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.DictBookGenre;

namespace Library.Common.ViewModels.Book
{
    public class BookDetailsModel
    {
        [Display(Name = "Book Id")]
        public int BookId { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ReleaseDate { get; set; }

        public string ISBN { get; set; }

        [Display(Name = "Book Genre")]
        public BookGenre BookGenre { get; set; }

        [Display(Name = "Count")]
        public int Count { get; set; }

        [Display(Name = "Count Left")]
        public int CountLeft { get; set; }

        [Display(Name = "Count Borrow")]
        public int CountBorrow { get; set; }

        [Display(Name = "Add Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AddDate { get; set; }

        [Display(Name = "Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ModifiedDate { get; set; }

        public IEnumerable<UserBorrowInfoModel> Borrow { get; set; }

        public IEnumerable<UserHistoryBorrowInfoModel> History { get; set; }
    }
}