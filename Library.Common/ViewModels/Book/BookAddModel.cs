﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Library.Common.ViewModels.DictBookGenre;
    
namespace Library.Common.ViewModels.Book
{
    public class BookAddModel
    {
        [Required]
        public string Author { get; set; }

        [Required]
        public string Title { get; set; }

        [Display(Name = "Relese Date")]
        [DataType(DataType.DateTime, ErrorMessage = "Zlituj sie!")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        
        public Nullable<DateTime> ReleaseDate { get; set; }

        [Required]
        [StringLength(13)]
        public string ISBN { get; set; }

        //[Range(1, 5, ErrorMessage="Porszę wybrać gatunek :D")]
        [Display(Name = "Book Genre")]
        [EnumDataType(typeof(BookGenre))]
        [Required]
        public BookGenre BookGenre { get; set; }

        [Display(Name = "Count")]
        [Range(0, int.MaxValue)]
        public int Count { get; set; }
    }
}