﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.Statistics
{
    public class BookStatisticsViewModel
    {
        public string Author { get; set; }

        public string Title { get; set; }

        public string GenerId { get; set; }

        [Display(Name = "Genre")]
        public string GenerName { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ReleaseDate { get; set; }
        [Display(Name = "Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime T { get; set; }
        [Display(Name = "Borrows Count")]
        public int BorrowsCount { get; set; }

    }
}