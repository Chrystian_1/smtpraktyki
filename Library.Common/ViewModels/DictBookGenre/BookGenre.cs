﻿using System;
using System.Linq;

namespace Library.Common.ViewModels.DictBookGenre
{
    public enum BookGenre
    {
        Texas = 1,
        Opera = 2,
        Ocean = 3,
        Winter = 4,
        Fire = 5
    }
}