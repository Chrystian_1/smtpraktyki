﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.ViewModels.Borrow
{
    public class BorrowBooksModel
    {
        public IDictionary<int, string> UsersById { get; set; }
        public IDictionary<int, string> BooksById { get; set; }
    }
}
