﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.Borrow
{
    public class BookBorrowedReturnModel
    {
        public int BorrowId { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        [Display(Name = "From Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Display(Name = "To Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

    }
}