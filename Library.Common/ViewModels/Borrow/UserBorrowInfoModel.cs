﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.Borrow
{
    public class UserBorrowInfoModel
    {
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; }
    }
}