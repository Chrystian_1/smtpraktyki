﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Library.Common.ViewModels.Borrow
{
    public class UserHistoryBorrowInfoModel
    {
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; }

        [Display(Name = "Is Returned")]
        public string GetIsReturned
        {
            get
            {
                return this.IsReturned ? "Returned" : "Borrowed";
            }
        }

        public bool IsReturned { private get; set; }
    }
}