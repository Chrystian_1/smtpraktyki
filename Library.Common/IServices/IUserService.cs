﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.User;

namespace Library.Common.IServices
{
    public interface IUserService
    {
        IEnumerable<UserViewModel> GetUsers();

        void DeleteUser(int userId);

        EditUserModel GetUser(int userId);

        void AddUser(NewUserModel newUser);

        UserDetails GetUserDetails(int userId);

        void EditUser(EditUserModel editUser);

        UserInfoDetailsModel GetBookInfoDetails(int id);

        IDictionary<int,string> GetAviableUsers();
        
        IQueryable<UserBorrowCountModel> GetUsersWithBooks();
    }
}