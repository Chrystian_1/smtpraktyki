﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Common.ViewModels.Book;
using Library.Common.ViewModels.Borrow;

namespace Library.Common.IServices
{
    public interface IBookService
    {
        IQueryable<BookViewModel> GetBooks();

        int AddBook(BookAddModel newBook);

        void EditBook(BookEditModel editBook);

        BookDetailsModel GetBookDetails(int id);

        BookEditModel GetBook(int id);

        IQueryable<UserBorrowInfoModel> GetBookStatus(int id);
        
        int GetBookBorrowCount(int id);

        IQueryable<UserHistoryBorrowInfoModel> GetBookHistory(int id);

        BookInfoDetailsModel GetBookInfoDetails(int id);

        IQueryable<BookBorrowedByUserModel> GetBorrowedBooks();

        IDictionary<int, string> GetAviableBooksForUser(int id);
    }
}