﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.ViewModels.Borrow;

namespace Library.Common.IServices
{
    public interface IBorrowService
    {
        void ReturnBook(int id);

        IQueryable<BookBorrowedReturnModel> GetBorrowedBooksByUser(int id);

        void ReturnBooks(int[] id);
        
        void BorrowBooks(int userId, int[] books);

    }
}
