﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.ViewModels.Statistics;

namespace Library.Common.IServices
{
    public interface IStatisticsService
    {
        IQueryable<BookStatisticsViewModel> GetBookOrderByBorrowsCount();

        IQueryable<UserStatisticsViewModel> GetUsersOrderByBorrowsCount();

        IQueryable<string> GetUsersLastsNames();

        IDictionary<int,string> GetGenres();
    }
}
