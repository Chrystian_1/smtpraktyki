﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common;
using Autofac;
using Library.DataAccess;

namespace Library.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        public LibraryDataEntities dbContext;

        public UnitOfWork()
        {
            this.dbContext = new LibraryDataEntities();
        }

        public int Commit()
        {
            return dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                    dbContext = null;
                }
            }
        }
    }
}
