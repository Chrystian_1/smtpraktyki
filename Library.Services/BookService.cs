﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Common;
using Library.Common.Exceptions;
using Library.Common.IServices;
using Library.Common.ViewModels.Book;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.DictBookGenre;
using Library.DataAccess;

namespace Library.Services
{
    public class BookService : IBookService
    {
        private UnitOfWork UoW;

        public BookService(UnitOfWork UoW)
        {
            this.UoW = UoW;
        }

        public IQueryable<BookViewModel> GetBooks()
        {
            {
                var context = this.UoW.dbContext;
                IQueryable<BookViewModel> query = (from book in context.Book
                                                   select new BookViewModel
                                                   {
                                                       BookId = book.BookId,
                                                       Author = book.Author,
                                                       ISBN = book.ISBN,
                                                       CountBorrow = ((from b in context.Borrow
                                                                       where b.BookId == book.BookId && b.IsReturned == false
                                                                       select b.BookId).Count()),
                                                       CountLeft = book.Count - ((from b in context.Borrow
                                                                                  where b.BookId == book.BookId && b.IsReturned == false
                                                                                  select b.BookId).Count()),
                                                       ModifiedDate = book.ModifiedDate,
                                                       ReleaseDate = book.ReleaseDate,
                                                       Title = book.Title,
                                                       AddDate = book.AddDate,
                                                       BookGenre = (BookGenre)book.DictBookGenre.BookGenreId,
                                                   });
                //List<BookViewModel> books = query.ToList();
                return query;
            }
        }

        public int AddBook(BookAddModel newBook)
        {
            var context = this.UoW.dbContext;
            {
                var entityBook =
                    new Book
                    {
                        Author = newBook.Author,
                        BookGenreId = (int)newBook.BookGenre,
                        Count = newBook.Count,
                        ReleaseDate = newBook.ReleaseDate,
                        ISBN = newBook.ISBN,
                        Title = newBook.Title,
                        AddDate = DateTime.Now
                    };
                context.Book.Add(entityBook);
                context.SaveChanges();
                return entityBook.BookId;
            }
        }

        public void EditBook(BookEditModel editBook)
        {
            var context = this.UoW.dbContext;
            {
                int count = (from b in context.Borrow
                             where b.BookId == editBook.BookId && b.IsReturned == false
                             select b.BookId).Count();
                if (count > editBook.Count)
                {
                    throw new EditBookException(count);
                }
                IQueryable<Book> query = from b in context.Book
                                         where b.BookId == editBook.BookId
                                         select b;
                Book book = query.Single();
                book.Author = editBook.Author;
                book.BookGenreId = (int)editBook.BookGenre;
                book.Count = editBook.Count;
                book.ISBN = editBook.ISBN;
                book.ReleaseDate = editBook.ReleaseDate;
                book.Title = editBook.Title;
                book.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }

        public BookDetailsModel GetBookDetails(int id)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<BookDetailsModel> query = context.Book
                                                            .Where(b => b.BookId == id)
                                                            .Select(b => new BookDetailsModel
                                                                   {
                                                                       AddDate = b.AddDate,
                                                                       Author = b.Author,
                                                                       BookGenre = (BookGenre)b.BookGenreId,
                                                                       BookId = b.BookId,
                                                                       Borrow = b.Borrow
                                                                                 .Where(bor => !bor.IsReturned)
                                                                                 .Select(bor => new UserBorrowInfoModel
                                                                                        {
                                                                                            UserName = bor.User.FirstName + " " + bor.User.LastName,
                                                                                            FromDate = bor.FromDate,
                                                                                            ToDate = bor.ToDate
                                                                                        }),
                                                                       Count = b.Count,
                                                                       CountBorrow = ((from borrow in context.Borrow
                                                                                       where borrow.IsReturned == true && borrow.BookId == b.BookId
                                                                                       select borrow.BookId)
                                                                                                            .Count()),
                                                                       CountLeft = b.Count - ((from borrow in context.Borrow
                                                                                               where borrow.BookId == b.BookId && borrow.IsReturned == false
                                                                                               select borrow.BookId).Count()),
                                                                       History = b.Borrow
                                                                                  .Where(bor => !bor.IsReturned)
                                                                                  .Select(bor => new UserHistoryBorrowInfoModel
                                                                                         {
                                                                                             FromDate = bor.FromDate,
                                                                                             UserName = bor.User.FirstName + " " + bor.User.LastName,
                                                                                             ToDate = bor.ToDate,
                                                                                             IsReturned = bor.IsReturned
                                                                                         }),
                                                                       ISBN = b.ISBN,
                                                                       ModifiedDate = b.ModifiedDate,
                                                                       ReleaseDate = b.ReleaseDate,
                                                                       Title = b.Title
                                                                   });
                BookDetailsModel book = query.Single();
                return book;
            }
        }

        public BookEditModel GetBook(int id)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<BookEditModel> query = (from b in context.Book
                                                   where b.BookId == id
                                                   select new BookEditModel
                                                   {
                                                       Author = b.Author,
                                                       BookGenre = (BookGenre)b.BookGenreId,
                                                       BookId = b.BookId,
                                                       Count = b.Count,
                                                       ISBN = b.ISBN,
                                                       ReleaseDate = b.ReleaseDate,
                                                       Title = b.Title,
                                                   });
                BookEditModel book = query.Single();
                return book;
            }
        }

        public int GetBookBorrowCount(int id)
        {
            var context = this.UoW.dbContext;
            {
                return (from b in context.Borrow
                        where b.BookId == id && b.IsReturned == false
                        select b.BookId).Count();
            }
        }

        public IQueryable<UserHistoryBorrowInfoModel> GetBookHistory(int id)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserHistoryBorrowInfoModel> query = from bo in context.Borrow
                                                               where bo.BookId == id
                                                               select new UserHistoryBorrowInfoModel
                                                               {
                                                                   UserName = bo.User.FirstName + " " + bo.User.LastName,
                                                                   FromDate = bo.FromDate,
                                                                   ToDate = bo.ToDate,
                                                                   IsReturned = bo.IsReturned
                                                               };
                //List<UserBorrowInfoModel> history = query.ToList();
                return query;
            }
        }

        public IQueryable<UserBorrowInfoModel> GetBookStatus(int id)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserBorrowInfoModel> query = from bo in context.Borrow
                                                        where bo.BookId == id && bo.IsReturned == false
                                                        select new UserBorrowInfoModel
                                                        {
                                                            UserName = bo.User.FirstName + " " + bo.User.LastName,
                                                            FromDate = bo.FromDate,
                                                            ToDate = bo.ToDate,
                                                        };
                //List<UserBorrowInfoModel> history = query.ToList();
                return query;
            }
        }

        public BookInfoDetailsModel GetBookInfoDetails(int id)
        {
            var context = this.UoW.dbContext;
            {
                var query = context.Book
                                   .Where(b => b.BookId == id)
                                   .Select(b => new BookInfoDetailsModel
                                          {
                                              AddDate = b.AddDate,
                                              Author = b.Author,
                                              BookGenre = b.DictBookGenre.Name,
                                              BookId = b.BookId,
                                              Count = b.Count,
                                              CountBorrow = ((from borrow in context.Borrow
                                                              where borrow.IsReturned == true && borrow.BookId == b.BookId
                                                              select borrow.BookId)
                                                                                   .Count()),
                                              CountLeft = b.Count - ((from borrow in context.Borrow
                                                                      where borrow.BookId == b.BookId && borrow.IsReturned == false
                                                                      select borrow.BookId).Count()),
                                              ISBN = b.ISBN,
                                              ModifiedDate = b.ModifiedDate,
                                              ReleaseDate = b.ReleaseDate,
                                              Title = b.Title
                                          });
                BookInfoDetailsModel book = query.Single();
                return book;
            }
        }


        public IDictionary<int, string> GetAviableBooksForUser(int id)
        {
            var context = this.UoW.dbContext;
            {
                var userQuery = from users in context.User
                                where users.UserId == id
                                select users.IsActive;

                if (userQuery.Single() == false)
                {
                    throw new UserActiveException();
                }
                
                var userBooksQuery = (from books in context.Book
                                      select books)
                                                   .Except(
                                                                                      from b in context.Borrow
                                                                                      where b.IsReturned == false && b.UserId == id
                                                                                      select b.Book)
                                                   .Select(c => new
                                                   {
                                                       c.BookId,
                                                       bookName = c.Author + ":" + c.Title
                                                   });

                var booksList = userBooksQuery.ToDictionary(o => o.BookId, o => o.bookName);

                return booksList;
            }
        }


        public IQueryable<BookBorrowedByUserModel> GetBorrowedBooks()
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<BookBorrowedByUserModel> query = (from book in context.Book
                                                             join borrow in context.Borrow on book.BookId equals borrow.BookId
                                                             where borrow.IsReturned == false
                                                             select new BookBorrowedByUserModel
                                                             {
                                                                 Author = book.Author,
                                                                 BorrowId = borrow.BorrowId,
                                                                 BookId = book.BookId,
                                                                 FirstName = borrow.User.FirstName,
                                                                 FromDate = borrow.FromDate,
                                                                 LastName = borrow.User.LastName,
                                                                 Title = book.Title,
                                                                 ToDate = borrow.ToDate,
                                                             }).Distinct();
                return query;
            }
        }
    }
}