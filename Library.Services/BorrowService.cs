﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Common.Exceptions;
using Library.Common.IServices;
using Library.Common.ViewModels.Borrow;
using Library.DataAccess;

namespace Library.Services
{
    public class BorrowService : IBorrowService
    {
        private UnitOfWork UoW;


        public BorrowService(UnitOfWork UoW)
        {
            this.UoW = UoW;
        }

        public void ReturnBook(int id)
        {
            var context = this.UoW.dbContext;
            {
                var query = from borrow in context.Borrow
                            where borrow.BorrowId == id
                            select borrow;
                var book = query.Single();
                book.IsReturned = true;
                context.SaveChanges();
            }
        }


        public IQueryable<Common.ViewModels.Borrow.BookBorrowedReturnModel> GetBorrowedBooksByUser(int id)
        {
            var context = this.UoW.dbContext;
            {
                var query = from borrow in context.Borrow
                            where borrow.UserId == id &&
                            borrow.IsReturned == false
                            select new BookBorrowedReturnModel
                            {
                                
                                Author = borrow.Book.Author,
                                BorrowId = borrow.BorrowId,
                                FromDate = borrow.FromDate,
                                ToDate = borrow.ToDate,
                                Title = borrow.Book.Title
                            };
                return query;
            }
        }


        public void ReturnBooks(int[] data)
        {
            var context = this.UoW.dbContext;
            {
                var query = (from borrow in context.Borrow
                            select borrow).Where(o=>data.Contains(o.BorrowId));
                foreach (var b in query)
                {
                    b.IsReturned = true;
                }
                context.SaveChanges();
            }
        }

        public void BorrowBooks(int userId, int[] books)
        {
            var context = this.UoW.dbContext;
            {
                var borrowsQuery = (from borrows in context.Borrow
                                    where borrows.UserId == userId && borrows.IsReturned==false
                                    select borrows.BookId).Where(o => books.Contains(o))
                            .Select(u => new { u });
                
                if (borrowsQuery.FirstOrDefault() != null)
                {
                    throw new BorrowException("pfff... you have this books");
                }
                
                var userQuery = (from users in context.User
                                where users.UserId == userId
                                select users.IsActive);

                if(userQuery.Single() != true){
                    throw new UserActiveException();
                }

                foreach (int b in books)
                {
                    context.Borrow.Add(new Borrow
                    {
                        BookId = b,
                        FromDate = DateTime.Now,
                        ToDate = DateTime.Now.AddDays(14),
                        UserId = userId,
                        IsReturned =false
                    });
                }
                context.SaveChanges();
            }
        }
    }
}
