﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using Library.Common.IServices;
using Library.Common.ViewModels.Statistics;

namespace Library.Services
{
    public class StatisticsService : IStatisticsService
    {
        private UnitOfWork UoW;

        public StatisticsService(UnitOfWork UoW) 
        {
            this.UoW = UoW;
        }

        public IQueryable<BookStatisticsViewModel> GetBookOrderByBorrowsCount()
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<BookStatisticsViewModel> query = (from books in context.Book
                                                             orderby books.Borrow.Count()
                                                             select new BookStatisticsViewModel
                                                             {
                                                                 Author = books.Author,
                                                                 ReleaseDate = books.ReleaseDate,
                                                                 Title = books.Title,
                                                                 GenerName = books.DictBookGenre.Name,
                                                                 GenerId = SqlFunctions.StringConvert((double)books.BookGenreId).Trim(),
                                                                 BorrowsCount = books.Borrow.Count(),
                                                                 T = DateTime.Now
                                                             }
                                                             
                                                             );
                return query;
            }
        }

        public IQueryable<UserStatisticsViewModel> GetUsersOrderByBorrowsCount()
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserStatisticsViewModel> query = (from users in context.User
                                                             orderby users.Borrow.Count()
                                                             select new UserStatisticsViewModel
                                                             {
                                                                 FirstName = users.FirstName,
                                                                 LastName = users.LastName,
                                                                 BorrowsCount = users.Borrow.Count()
                                                             });
                return query;
            }
        }

        public IQueryable<string> GetUsersLastsNames()
        {
            var context = this.UoW.dbContext;
            {
                var query = (from users in context.User
                             select users.FirstName);
                return query;
            }
        }


        public IDictionary<int, string> GetGenres()
        {
            var context = this.UoW.dbContext;
            {
                var genresList = (from genres in context.DictBookGenre
                             select new
                             {
                                 genres.Name,
                                 genres.BookGenreId
                             }).ToDictionary(o => o.BookGenreId, n => n.Name);
                return genresList;
            }
        }
    }
}