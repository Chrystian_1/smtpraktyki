﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Common.IServices;
using Library.Common.ViewModels.Borrow;
using Library.Common.ViewModels.User;
using Library.DataAccess;

namespace Library.Services
{
    public class UserService : IUserService
    {
        private UnitOfWork UoW;

        public UserService(UnitOfWork UoW)
        {
            this.UoW = UoW;
        }

        public IEnumerable<UserViewModel> GetUsers()
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserViewModel> query = (from u in context.User
                                                   select new UserViewModel
                                                   {
                                                       UserId = u.UserId,
                                                       FirstName = u.FirstName,
                                                       LastName = u.LastName,
                                                       BirthDate = u.BirthDate,
                                                       Email = u.Email,
                                                       Phone = u.Phone,
                                                       AddDate = u.AddDate,
                                                       ModifiedDate = u.ModifiedDate,
                                                       IsActive = u.IsActive
                                                   });
                List<UserViewModel> users = query.ToList();
                return users;
            }
        }

        public void DeleteUser(int userId)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<User> query = from u in context.User
                                         where u.UserId == userId
                                         select u;
                User user = query.Single();
                user.IsActive = false;
                context.SaveChanges();
            }
        }

        public EditUserModel GetUser(int userId)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<EditUserModel> query = (from u in context.User
                                                   where u.UserId == userId
                                                   select new EditUserModel
                                                   {
                                                       UserId = u.UserId,
                                                       FirstName = u.FirstName,
                                                       LastName = u.LastName,
                                                       BirthDate = u.BirthDate,
                                                       Email = u.Email,
                                                       Phone = u.Phone,
                                                       IsActive = u.IsActive
                                                   });
                EditUserModel user = query.Single();
                return user;
            }
        }

        public void AddUser(NewUserModel newUser)
        {
            var context = this.UoW.dbContext;
            {
                var entityUser =
                    new User
                    {
                        FirstName = newUser.FirstName,
                        LastName = newUser.LastName,
                        BirthDate = newUser.BirthDate,
                        Email = newUser.Email,
                        Phone = newUser.Phone,
                        AddDate = DateTime.Now,
                        IsActive = newUser.IsActive,
                    };
                context.User.Add(entityUser);
                context.SaveChanges();
            }
        }

        public UserDetails GetUserDetails(int userId)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserDetails> query = context.User
                                                       .Where(u => u.UserId == userId)
                                                       .Select(u => new UserDetails
                                                              {
                                                                  AddDate = u.AddDate,
                                                                  BirthDate = u.BirthDate,
                                                                  Email = u.Email,
                                                                  FirstName = u.FirstName,
                                                                  IsActive = u.IsActive,
                                                                  LastName = u.LastName,
                                                                  ModifiedDate = u.ModifiedDate,
                                                                  Phone = u.Phone,
                                                                  UserId = u.UserId,
                                                                  Borrow = u.Borrow
                                                                            .Where(b => !b.IsReturned)
                                                                            .Select(b => new BookBorrowInfo
                                                                                   {
                                                                                       Author = b.Book.Author,
                                                                                       Title = b.Book.Title,
                                                                                       FromDate = b.FromDate,
                                                                                       ToDate = b.ToDate,
                                                                                       IsReturned = b.IsReturned
                                                                                   }),
                                                                  History = u.Borrow
                                                                             .Where(b => b.IsReturned)
                                                                             .Select(b => new BookBorrowInfo
                                                                                    {
                                                                                        Author = b.Book.Author,
                                                                                        Title = b.Book.Title,
                                                                                        FromDate = b.FromDate,
                                                                                        ToDate = b.ToDate,
                                                                                        IsReturned = b.IsReturned
                                                                                    })
                                                              });

                UserDetails user = query.Single();
                return user;
            }
        }

        public void EditUser(EditUserModel editUser)
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<User> query = from u in context.User
                                         where u.UserId == editUser.UserId
                                         select u;
                User user = query.Single();
                user.LastName = editUser.LastName;
                user.FirstName = editUser.FirstName;
                user.Phone = editUser.Phone;
                user.Email = editUser.Email;
                user.BirthDate = editUser.BirthDate;
                user.IsActive = editUser.IsActive;
                user.ModifiedDate = DateTime.Now;
                context.SaveChanges();
            }
        }

        public UserInfoDetailsModel GetBookInfoDetails(int id)
        {
            var context = this.UoW.dbContext;
            {
                var query = from u in context.User
                            where u.UserId == id
                            select new UserInfoDetailsModel
                            {
                                AddDate = u.AddDate,
                                BirthDate = u.BirthDate,
                                Email = u.Email,
                                FirstName = u.FirstName,
                                IsActive = u.IsActive,
                                LastName = u.LastName,
                                ModifiedDate = u.ModifiedDate,
                                Phone = u.Phone,
                                UserId = u.UserId
                            };
                var user = query.Single();
                return user;

            }
        }


        public IDictionary<int, string> GetAviableUsers()
        {
            var context = this.UoW.dbContext;
            {
                var userQuery = (from users in context.User
                                 where users.IsActive == true
                                 select new
                                 {
                                     users.UserId,
                                     userName = users.LastName + " " + users.FirstName
                                 }
                                 );
                var usersList = userQuery.ToDictionary(o => o.UserId, o => o.userName);
                return usersList;
            }
        }



        public IQueryable<UserBorrowCountModel> GetUsersWithBooks()
        {
            var context = this.UoW.dbContext;
            {
                IQueryable<UserBorrowCountModel> query =
                    (from user in context.User
                     select new UserBorrowCountModel
                     {
                         UserId = user.UserId,
                         FirstName = user.FirstName,
                         LastName = user.LastName,
                         Count = (from borrow in context.Borrow
                                  where borrow.UserId == user.UserId &&
                                        borrow.IsReturned == false
                                  select new
                                  {
                                      borrow.UserId
                                  })
                                    .Count(),
                     });
                return query;
            }
        }
    }
}